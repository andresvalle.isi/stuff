#!/usr/bin/perl
use strict;
use Data::Dumper;

my $to_search = $ARGV[0]; # eg. "RESDAN"

if( !$to_search ){
    print "Please enter a name to search!" . "\n";
    exit 1;
}

print "Processing names..." . "\n";
my $procesed = procesar();

print "Searching name for $to_search..." . "\n";
my $matches = find($to_search);

if( scalar @$matches ){
    print "Matches for $to_search: " . join (',', @$matches) . "\n";
}else{
    print "No matches found =(" . "\n";
}

exit 0;

sub find {
    my $to_search = shift;
    return $procesed->{ normalize_str(uc($to_search)) };
}

sub procesar {
    my $names = read_file( 'names.data' );

    my $catalog = {};
    foreach my $name ( @$names ){
        my $normalized_str = normalize_str($name);
        if( exists $catalog->{ $normalized_str } ){
            push @{$catalog->{ $normalized_str }}, $name;
        }else{
            $catalog->{ $normalized_str } = [ $name ];
        }
    }

    return $catalog;
}

sub normalize_str {
    my $string = shift;
    return join '', sort split //, $string;
}

sub read_file {
    my $filename = shift;
    my $data = [];
    if (open(my $fh, '<:encoding(UTF-8)', $filename)) {
        while (my $line = <$fh>) {
            chomp $line;
            push @$data, $line;
        }
    }
    return $data;
}
